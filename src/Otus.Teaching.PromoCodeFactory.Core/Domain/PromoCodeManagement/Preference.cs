﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        public string Name { get; set; }

       // public List<Customer> Customers { get; set; }

        public virtual List<CustomerPreference> CustomerPreferences { get; set; }

        public virtual List<PromoCode> PromoCodes { get; set; }
    }
}