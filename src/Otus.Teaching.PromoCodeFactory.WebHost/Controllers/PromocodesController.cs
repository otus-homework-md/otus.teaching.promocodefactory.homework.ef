﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Castle.Core.Resource;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Context;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<CustomerPreference> _customerPreferenceRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Employee> _employeeRepository;


        public PromocodesController(
            IRepository<Customer> customerRepository,            
            IRepository<Preference> preferenceRepository,
            IRepository<CustomerPreference> customerPreferenceReposiory,
            IRepository<PromoCode> promocodeRepository,
            IRepository<Employee> empRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _customerPreferenceRepository = customerPreferenceReposiory;
            _promoCodeRepository = promocodeRepository;
            _employeeRepository = empRepository;    
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promoCodeRepository.GetAllAsync();

            var promocodesModelList = promocodes.Select(x =>
                new PromoCodeShortResponse()
                {
                    Id = x.Id,
                    Code = x.Code,
                    ServiceInfo = x.ServiceInfo,
                    BeginDate = x.BeginDate.ToString(),
                    EndDate=x.EndDate.ToString(),
                    PartnerName= x.PartnerName

                }).ToList();

            return promocodesModelList;
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var partnerManagers= await _employeeRepository.GetAllAsync();
            var partnerManager=partnerManagers.FirstOrDefault();
            if (partnerManager == null) return NotFound("Отсутствуют партнерские менеджеры");

            
            _=Guid.TryParse(request.Preference, out Guid prefId);

            var preference=await _preferenceRepository.GetByIdAsync(prefId);
            if (preference == null) return NotFound("Отсутствуют предпочтение");

            var customerPreferences=await _customerPreferenceRepository.GetAllAsync();
            var customerId=customerPreferences.Where(x=>x.PreferenceId==prefId).FirstOrDefault().CustomerId;

            var promocode= new PromoCode() 
            {
                Id= Guid.NewGuid(),
                Code=request.PromoCode,
                ServiceInfo=request.ServiceInfo,
                BeginDate=DateTime.Now,
                EndDate=new DateTime(2024,1,1),
                PartnerName= request.PartnerName,
                EmployeeId=partnerManager.Id,
                PreferenceId=prefId,
                CustomerId=customerId,
            };

            var res = await _promoCodeRepository.CreateAsync(promocode);
            return Ok(res);
        }
    }
}