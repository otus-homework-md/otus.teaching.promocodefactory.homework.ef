﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context
{
    public class PromoCodeFactoryContext : DbContext
    {
        public PromoCodeFactoryContext(DbContextOptions<PromoCodeFactoryContext> options)
           : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new RolesConfiguration());
            modelBuilder.ApplyConfiguration(new EmployeesConfiguration());

            //modelBuilder.Entity<Customer>().UseTpcMappingStrategy();

            //modelBuilder.Entity<Customer>()
            //    .HasMany<Preference>(s => s.Preferences)
            //    .WithMany(c => c.Customers)
            //    .UsingEntity<CustomerPreference>(
            //    j => j
            //    .HasOne(pt => pt.Preference)
            //    .WithMany(t => t.CustomerPreferences)
            //    .HasForeignKey(pt => pt.PreferenceId),
            //    j => j
            //        .HasOne(pt => pt.Customer)
            //        .WithMany(p => p.CustomerPreferences)
            //        .HasForeignKey(pt => pt.CustomerId)
            //    );


            modelBuilder.ApplyConfiguration(new CustomerPreferenceConfiguration());

            modelBuilder.ApplyConfiguration(new PreferencesConfiguration());
            modelBuilder.ApplyConfiguration(new CustomersConfiguration());
            modelBuilder.ApplyConfiguration(new PromoCodesConfiguration());

            

        }
    }
}
