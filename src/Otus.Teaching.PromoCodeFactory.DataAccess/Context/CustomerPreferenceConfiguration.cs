﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context
{
    public class CustomerPreferenceConfiguration:IEntityTypeConfiguration<CustomerPreference>
    {
        public void Configure(EntityTypeBuilder<CustomerPreference> builder)
        {
            builder.HasKey(t => new { t.PreferenceId, t.CustomerId }); ;
            
            builder.HasOne(pt => pt.Preference)
                .WithMany(t => t.CustomerPreferences)
                .HasForeignKey(pt => pt.PreferenceId);

            builder
                    .HasOne(pt => pt.Customer)
                    .WithMany(p => p.CustomerPreferences)
                    .HasForeignKey(pt => pt.CustomerId);

            // Наполнение тестовыми данными
            builder.HasData(FakeDataFactory.CustomerPreferences);
        }
    }
}
