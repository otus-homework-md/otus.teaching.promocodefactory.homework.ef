﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Castle.Core.Resource;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Context;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<CustomerPreference> _customerPreferenceRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly PromoCodeFactoryContext _dbc;

        public CustomersController(
            IRepository<Customer> customerRepository, 
            PromoCodeFactoryContext pcfc, 
            IRepository<Preference> preferenceRepository, 
            IRepository<CustomerPreference> customerPreferenceReposiory,
            IRepository<PromoCode> promocodeRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository= preferenceRepository;
            _customerPreferenceRepository = customerPreferenceReposiory;
            _promoCodeRepository= promocodeRepository;
            _dbc = pcfc;
        }

        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var customerModelList = customers.Select(x =>
                new CustomerShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FirstName = x.FullName,
                    LastName= x.LastName
                }).ToList();

            return customerModelList;
        }

        /// <summary>
        /// Получить данные клиента по ID
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            var customerPreferences= new List<PreferenceShortResponce>();
            foreach(var customerPreference in customer.CustomerPreferences)
            {
                var preferenceShResp= new PreferenceShortResponce() 
                {
                    Id= customerPreference.Preference.Id,
                    Name= customerPreference.Preference.Name,
                };
                customerPreferences.Add(preferenceShResp);
            }

            var customerPromoCodes = new List<PromoCodeShortResponse>();
            foreach (var customerPromoCode in customer.PromoCodes)
            {
                var promocodeShResp = new PromoCodeShortResponse()
                {
                    Id = customerPromoCode.Id,
                    Code= customerPromoCode.Code,
                    ServiceInfo=customerPromoCode.ServiceInfo,
                    BeginDate= customerPromoCode.BeginDate.ToString(), 
                    EndDate= customerPromoCode.EndDate.ToString(),
                    PartnerName= customerPromoCode.PartnerName
                };
                customerPromoCodes.Add(promocodeShResp);
            }

            var customerModel = new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName= customer.LastName,
                Preferences= customerPreferences,
                PromoCodes= customerPromoCodes
               
            };

            return customerModel;
        }

        /// <summary>
        /// Создать нового клиента , вместе с предпочтениями
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            try
            {

                var customerPreferences = new List<CustomerPreference>();
                var customerId = Guid.NewGuid();

                foreach (var pId in request.PreferenceIds)
                {                   
                    var pref=await _preferenceRepository.GetByIdAsync(pId);
                    if(pref != null)
                    {
                        var cusPref = new CustomerPreference()
                        {
                            Id = Guid.NewGuid(),
                            CustomerId = customerId,
                            PreferenceId = pref.Id
                        };
                        customerPreferences.Add(cusPref);
                    }
                    
                }

                var customer = new Customer()
                {
                    Id = customerId,
                    Email = request.Email,
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    CustomerPreferences = customerPreferences

                };

                var res = await _customerRepository.CreateAsync(customer);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return  StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Обновить данные о клиенте, в том числе и о его предпочтениях
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            try
            {
                var customer=await _customerRepository.GetByIdAsync(id);

                if(customer == null) return NotFound("По указанному Id пользователь не найден");

                var customerPreferences = customer.CustomerPreferences;
                                
                foreach (var pId in request.PreferenceIds)
                {
                    var pref = await _preferenceRepository.GetByIdAsync(pId);
                    if (pref != null)
                    {
                        if (!customerPreferences.Where(x => x.CustomerId == id && x.PreferenceId == pId).Any()) continue;    
                        var cusPref = new CustomerPreference()
                        {
                            Id = Guid.NewGuid(),
                            CustomerId = id,
                            PreferenceId = pId
                        };
                        customerPreferences.Add(cusPref);
                    }

                }

                customer.CustomerPreferences = customerPreferences;
                customer.Email=request.Email ;
                customer.FirstName=request.FirstName ;
                customer.LastName=request.LastName ;
                               

                var res = await _customerRepository.UpdateAsync(customer);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Удалить клиента и выданные ему промокоды
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            try
            {
                var customer = await _customerRepository.GetByIdAsync(id);

                if (customer == null) return NotFound("По указанному Id пользователь не найден");

                foreach(var promoCode in customer.PromoCodes)
                {
                    await _promoCodeRepository.RemoveAsync(promoCode);
                }

                foreach (var customerPref in customer.CustomerPreferences)
                {
                    await _customerPreferenceRepository.RemoveAsync(customerPref);
                }


                var res = await _customerRepository.RemoveAsync(customer);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}