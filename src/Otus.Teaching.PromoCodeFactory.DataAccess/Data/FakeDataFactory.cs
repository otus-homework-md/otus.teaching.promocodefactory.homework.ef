﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                RoleId= Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                //Role = Roles.FirstOrDefault(x => x.Name == "Admin"),
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                RoleId= Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                //Role = Roles.FirstOrDefault(x => x.Name == "PartnerManager"),
                AppliedPromocodesCount = 10
            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };

        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
        };

        public static IEnumerable<Customer> Customers
        {
            get
            {
                var customerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = customerId,
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                        //TODO: Добавить предзаполненный список предпочтений
                        //Preferences= new List<Preference>()
                        //{
                        //    FakeDataFactory.Preferences.FirstOrDefault(P=>P.Name=="Театр" ),
                        //    FakeDataFactory.Preferences.FirstOrDefault(P=>P.Name=="Семья" )
                        //}
                    }
                };

                return customers;
            }
        }

        public static IEnumerable<CustomerPreference> CustomerPreferences
        {
            get
            {
                var customerPreferenceId1 = Guid.Parse("a6c8c6b1-4241-45b0-ab31-244740aaf0f0");
                var customerPreferenceId2 = Guid.Parse("a6c1c6b1-1241-45b0-ab31-244740aaf0f0");
                var customerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
                var preferenceId1 = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c");
                var preferenceId2 = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd");
                var customersPreferences = new List<CustomerPreference>()
                {
                    new CustomerPreference()
                    {
                        Id = customerPreferenceId1,
                        CustomerId = customerId,
                        PreferenceId = preferenceId1
                    },
                    new CustomerPreference()
                    {
                        Id = customerPreferenceId2,
                        CustomerId = customerId,
                        PreferenceId = preferenceId2
                    }
                };

                return customersPreferences;
            }
        }

    }
}