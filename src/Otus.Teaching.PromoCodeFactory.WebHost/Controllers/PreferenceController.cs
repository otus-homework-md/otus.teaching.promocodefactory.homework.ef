﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : ControllerBase
    {
        private readonly IRepository<Preference> _preferenceRepository;

        public PreferenceController(IRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить данны о перечне предпочтений клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PreferenceShortResponce>> GetCustomersAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();

            var customerModelList = preferences.Select(x =>
                new PreferenceShortResponce()
                {
                    Id= x.Id,
                     Name = x.Name
                }).ToList();

            return customerModelList;
        }
    }
}
