﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context
{
    public class PromoCodesConfiguration : IEntityTypeConfiguration<PromoCode>
    {
        public void Configure(EntityTypeBuilder<PromoCode> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Code).IsRequired().HasMaxLength(100);

            builder.HasOne(p => p.PartnerManager)
            .WithMany(p => p.PromoCodes)
            .HasForeignKey(i => i.EmployeeId);

            builder.HasOne(p => p.Customer)
            .WithMany(p => p.PromoCodes)
            .HasForeignKey(i => i.CustomerId);
            ///.OnDelete(DeleteBehavior.SetNull);

            builder.HasOne(p => p.Preference)
            .WithMany(p => p.PromoCodes)
            .HasForeignKey(i => i.PreferenceId);

            // Наполнение тестовыми данными
            //builder.HasData(FakeDataFactory.Roles);
        }
    }
}
